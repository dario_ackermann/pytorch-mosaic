import pickle

from PIL import Image
import numpy as np
import utils
from assignment1 import Assignment1


class ImageToMosaic(Assignment1):

    def __init__(self):
        self.data = pickle.load(open('./features/cifar10/raw.pkl', 'rb'))
        self.features = None
        self.nn = self.get_model()
        self.model_file = './models/nearest_neighbor.pkl'
        self.feature_file = './features/cifar10/avg.pkl'
        super(ImageToMosaic, self).__init__()


    def convertWithPath(self, path_to_image):

        img = Image.open(path_to_image).convert('RGB')
        img = utils.resize_proportional(img, 2000)
        target_image = np.array(img) / 255

        self.encode_features(False)
        self.train(True)

        output_img = self.mosaic(target_image)

        output_img *= 255
        im = Image.fromarray(output_img.astype('uint8'))
        save_location = utils.datetime_filename('./output/ITM/mosaic.png');
        im.save(save_location)

        return save_location